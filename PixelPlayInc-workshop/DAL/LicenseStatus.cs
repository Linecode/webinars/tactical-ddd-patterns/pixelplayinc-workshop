namespace PixelPlayInc_workshop.DAL;

public enum LicenseStatus
{
    Provisioned,
    Active,
    Expired
}