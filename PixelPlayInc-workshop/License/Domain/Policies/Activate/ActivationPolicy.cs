using PixelPlayInc.Common;

namespace PixelPlayInc_workshop.License.Domain.Policies.Activate;

public interface ActivationPolicy
{
    Result Activate(License license);
}