namespace PixelPlayInc_workshop.DAL;

public class License : BaseEntity
{
    public LicenseStatus Status { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public Guid LicensorId { get; set; }
    public int Territory { get; set; }
    public string Key { get; set; }
}