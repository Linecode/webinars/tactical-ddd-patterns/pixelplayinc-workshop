namespace PixelPlayInc_workshop.DAL;

public abstract class BaseEntity
{
    public Guid Id { get; set; }
}