using PixelPlayInc_workshop.License.Application.Port;
using PixelPlayInc_workshop.License.Domain.Licensors;

namespace PixelPlayInc_workshop.License.Infrastructure.Persistence.Licensors;

public class LicensorProvider : Dictionary<Guid, Licensor>, ILicensorProvider
{
    public Task<Licensor> GetOrCreate(LicensorIdOrData licensorIdOrData)
    {
        if (licensorIdOrData.LicensorId.HasValue)
        {
            if (ContainsKey(licensorIdOrData.LicensorId.Value))
            {
                return Task.FromResult(this[licensorIdOrData.LicensorId.Value]);
            }
        }

        var id = Guid.NewGuid();
        var licensor = new Licensor(id, licensorIdOrData.Name!, licensorIdOrData.Address!);
        
        Add(licensor.Id, licensor);

        return Task.FromResult(licensor);
    }
}
