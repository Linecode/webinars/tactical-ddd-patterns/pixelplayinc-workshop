using PixelPlayInc_workshop.License.Domain.Licensors;

namespace PixelPlayInc_workshop.License.Application.Port;

public interface ILicensorProvider
{
    Task<Domain.Licensors.Licensor> GetOrCreate(LicensorIdOrData licensorIdOrData);
}