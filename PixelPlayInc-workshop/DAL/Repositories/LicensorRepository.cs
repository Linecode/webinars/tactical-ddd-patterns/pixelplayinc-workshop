namespace PixelPlayInc_workshop.DAL.Repositories;

public class LicensorRepository : Dictionary<Guid, Licensor>, ILicensorRepository
{
    public Task Add(Licensor licensor, CancellationToken cancellationToken = default)
    {
        Add(licensor.Id, licensor);
        
        return Task.CompletedTask;
    }

    public Task<Licensor?> Get(Guid id, CancellationToken cancellationToken = default)
    {
        var exists = TryGetValue(id, out var licensor);
        
        return Task.FromResult(exists? licensor : null);
    }
}