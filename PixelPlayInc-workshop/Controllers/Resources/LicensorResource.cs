namespace PixelPlayInc_workshop.Controllers.Resources;

public class LicensorResource
{
    public Guid? Id { get; set; }
    public string Name { get; set; }
    public string Address { get; set; }
}