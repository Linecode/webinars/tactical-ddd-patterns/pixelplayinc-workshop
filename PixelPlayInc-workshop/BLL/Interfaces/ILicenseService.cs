using PixelPlayInc_workshop.BLL.Dtos;
using PixelPlayInc_workshop.DAL;

namespace PixelPlayInc_workshop.BLL.Interfaces;

public interface ILicenseService
{
    Task<Guid> Create(LicenseDto licenseDto);
    Task Activate(Guid licenseId);
    Task CloseLicense(Guid licenseId);
    Task<DAL.License> Get(Guid licenseId);
}