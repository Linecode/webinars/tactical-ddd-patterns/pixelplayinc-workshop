using PixelPlayInc_workshop.BLL;
using PixelPlayInc_workshop.BLL.Interfaces;
using PixelPlayInc_workshop.DAL.Repositories;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

// repositories
builder.Services.AddSingleton<ILicenseRepository, LicenseRepository>();
builder.Services.AddSingleton<ILicensorRepository, LicensorRepository>();

// Services
builder.Services.AddScoped<ILicenseService, LicenseService>();
builder.Services.AddScoped<ILicensorService, LicensorService>();
builder.Services.AddScoped<ILibraryService, LibraryService>();

// Facades
builder.Services.AddScoped<ILicenceFacade, LicenceFacade>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();