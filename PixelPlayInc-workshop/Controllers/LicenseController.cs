using Microsoft.AspNetCore.Mvc;
using PixelPlayInc_workshop.BLL;
using PixelPlayInc_workshop.BLL.Dtos;
using PixelPlayInc_workshop.Controllers.Resources;

namespace PixelPlayInc_workshop.Controllers;

[ApiController]
[Route("api/[controller]")]
public class LicenseController : ControllerBase
{
    private readonly ILicenceFacade _licenceFacade;

    public LicenseController(ILicenceFacade licenceFacade)
    {
        _licenceFacade = licenceFacade;
    }

    [HttpPost]
    public async Task<IActionResult> CreateLicense(CreateLicenseResource resource)
    {
        var licenseDto = new LicenseDto
        {
            Key = resource.DecryptionKey,
            StartDate = resource.StartDate,
            EndDate = resource.EndDate,
            LicensorId = resource.LicensorId,
            Territory = resource.Territory
        };
        var licensorDto = new LicensorDto
        {
            Id = resource.Licensor.Id,
            Name = resource.Licensor.Name,
            Address = resource.Licensor.Address
        };
        
        var id = await _licenceFacade.CreateLicense(licenseDto, licensorDto);
        
        return Created($"/api/license/{id}", null);
    }

    [HttpPost("{id}/activate")]
    public async Task<IActionResult> ActivateLicense(Guid id)
    {
        await _licenceFacade.ActivateLicense(id);

        return Ok();
    }

    [HttpPost("{id}/expire")]
    public async Task<IActionResult> CloseLicense(Guid id)
    {
        await _licenceFacade.CloseLicense(id);

        return Ok();
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetLicense(Guid id)
    {
        var licenseDto = await _licenceFacade.GetLicense(id);

        return Ok(licenseDto);
    }
}