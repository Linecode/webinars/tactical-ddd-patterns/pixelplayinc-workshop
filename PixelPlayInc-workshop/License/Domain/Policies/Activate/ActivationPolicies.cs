using PixelPlayInc.Common;

namespace PixelPlayInc_workshop.License.Domain.Policies.Activate;

public static class ActivationPolicies
{
    public static ActivationPolicy Default =>
        new CompositePolicy(new KeyMustBeDefinedPolicy(), new DateRangeMustBeDefinedPolicy());
    
    private class KeyMustBeDefinedPolicy : ActivationPolicy
    {
        public Result Activate(License license)
            => string.IsNullOrWhiteSpace(license.Key.Value) ? Result.Error(new Exception()) : Result.Success();
    }
    
    private class DateRangeMustBeDefinedPolicy : ActivationPolicy
    {
        public Result Activate(License license)
            => license.DateRange != null ? Result.Success() : Result.Error(new Exception());
    }
    
    private class AlwaysTruePolicy : ActivationPolicy
    {
        public Result Activate(License license)
            => Result.Success();
    }

    private class CompositePolicy : ActivationPolicy
    {
        private readonly ActivationPolicy[] _policies;

        public CompositePolicy(params ActivationPolicy[] policies)
        {
            _policies = policies;
        }

        public Result Activate(License license)
            => _policies.Select(x => x.Activate(license))
                .Where(x => !x.IsSuccessful)
                .DefaultIfEmpty()
                .First();
    }
}