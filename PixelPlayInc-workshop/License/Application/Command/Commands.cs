using PixelPlayInc_workshop.Common;
using PixelPlayInc_workshop.License.Domain;
using PixelPlayInc_workshop.License.Domain.Licensors;

namespace PixelPlayInc_workshop.License.Application.Command;

public abstract record Commands
{
    public record CreateLicenseCommand(
        DateRange DateRange, 
        DecryptionKey DecryptionKey, 
        LicensorIdOrData LicensorIdOrData, 
        RegionIdentifier Region) : Domain.License.Commands;
}