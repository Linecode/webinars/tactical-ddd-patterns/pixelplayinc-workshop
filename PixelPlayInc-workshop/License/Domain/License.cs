using Microsoft.AspNetCore.Authentication.BearerToken;
using PixelPlayInc_workshop.Common;
using PixelPlayInc_workshop.DAL;
using PixelPlayInc_workshop.License.Domain.Policies.Activate;
using PixelPlayInc.Common;

namespace PixelPlayInc_workshop.License.Domain;

public sealed class Aggr : AggregateRoot<Guid>
{
    public Guid Id { get; }
}

public sealed class License : Entity<Guid>, AggregateRoot<Guid>
{
    public enum Statuses { Provisioned, Active, Expired }
    public Statuses Status { get; private set; } = Statuses.Provisioned;
    public DateRange DateRange { get; private set; } = null!;
    public Guid LicensorId { get; private set; }
    public RegionIdentifier Region { get; private set; } = null!;
    public DecryptionKey Key { get; private set; } = null!;
    
    [Obsolete("Only for ORM", true)]
    private License() { }

    private License(Guid id)
    {
        Id = id;
    }

    public IEnumerable<DomainEvent> Activate(Commands.ActivateCommand command)
    {
        if (Status != Statuses.Provisioned)
            throw new Exception("License already activated");
        
        if (DateRange.IsWithinRange(command.Now))
            throw new Exception("License not active yet");

        var result = command.policy.Activate(this);

        // result.Match(() =>
        // {
        //     
        // })

        var @event = new Events.LicenseActivated(Guid.NewGuid(), Id);
        Apply(@event);

        return new[] { @event };
    }

    private void Apply(Events.LicenseActivated _)
    {
        Status = Statuses.Active;
    }

    // public static License Create(Commands.CreateCommand command)
    // {
    //     var (dateRange, key, licensor, region) = command;
    //     var id = Guid.NewGuid();
    //     var @event = new Events.LicenseCreated(
    //         Guid.NewGuid(), 
    //         id, 
    //         dateRange,
    //         key.Value,
    //         licensor.Id,
    //         region);
    //
    //     var license = new License(id);
    //     license.Apply(@event);
    //
    //     return license;
    // }
    
    public static (License, DomainEvent) Create(Commands.CreateCommand command)
    {
        var (dateRange, key, licensor, region) = command;
        var id = Guid.NewGuid();
        var @event = new Events.LicenseCreated(
            Guid.NewGuid(), 
            id, 
            dateRange,
            key.Value,
            licensor.Id,
            region);

        var license = new License(id);
        license.Apply(@event);

        return (license, @event);
    }

    private void Apply(Events.LicenseCreated @event)
    {
        DateRange = @event.DateRange;
        LicensorId = @event.LicensorId;
        Region = @event.Region;
        Key = DecryptionKey.Parse(@event.DecryptionKey);
    }

    public abstract record Commands
    {
        public record CreateCommand(DateRange DateRange, DecryptionKey DecryptionKey, Domain.Licensors.Licensor Licensor,
            RegionIdentifier Region) : Commands;

        public record ActivateCommand(DateTime Now, ActivationPolicy policy) : Commands;

        public record ExpireCommand() : Commands;
    }

    public abstract record Events : DomainEvent
    {
        public record LicenseCreated(
            Guid Id, 
            Guid LicenseId, 
            DateRange DateRange, 
            string DecryptionKey, 
            Guid LicensorId,
            RegionIdentifier Region) : Events;

        public record LicenseActivated(Guid Id, Guid LicenseId) : Events;

        public record Expired(Guid Id) : Events;
    }
}