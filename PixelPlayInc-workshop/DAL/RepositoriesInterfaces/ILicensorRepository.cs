namespace PixelPlayInc_workshop.DAL.Repositories;

public interface ILicensorRepository
{
    Task Add(Licensor licensor, CancellationToken cancellationToken = default);
    Task<Licensor> Get(Guid id, CancellationToken cancellationToken = default);
}