namespace PixelPlayInc_workshop.DAL.Repositories;

public class LicenseRepository : Dictionary<Guid, License>, ILicenseRepository
{
    public Task Add(License license, CancellationToken cancellationToken = default)
    {
        Add(license.Id, license);
        
        return Task.CompletedTask;
    }

    public Task Update(License license, CancellationToken cancellationToken = default)
    {
        this[license.Id] = license;
        
        return Task.CompletedTask;
    }

    public Task<License?> Get(Guid id, CancellationToken cancellationToken = default)
    {
        var exists = TryGetValue(id, out var license);
        
        return Task.FromResult(exists? license : null);
    }
}