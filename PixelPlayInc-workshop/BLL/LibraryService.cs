using PixelPlayInc_workshop.DAL;

namespace PixelPlayInc_workshop.BLL;

public class LibraryService : ILibraryService
{
    private readonly ILogger<LibraryService> _logger;

    public LibraryService(ILogger<LibraryService> logger)
    {
        _logger = logger;
    }
    
    public Task OnLicenseActivated(DAL.License license)
    {
        _logger.LogInformation("License activated: {LicenseId}", license.Id);
        
        return Task.CompletedTask;
    }
}