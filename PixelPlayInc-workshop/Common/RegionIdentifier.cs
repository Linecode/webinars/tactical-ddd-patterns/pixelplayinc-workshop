using PixelPlayInc.Common;

namespace PixelPlayInc_workshop.Common;

// ISO 3166
public class RegionIdentifier : ValueObject<RegionIdentifier>
{
    public ushort Value { get; private set; } = ushort.MinValue;
    
    private RegionIdentifier()
    {
    }
    
    public RegionIdentifier(ushort id)
    {
        if (id > 999) throw new ArgumentOutOfRangeException(nameof(id), "Id must be between 0 and 999");
        Value = id;
    }

    public RegionIdentifier(int id) : this((ushort)id) { }

    public override string ToString()
        => Value.ToString().PadLeft(3, '0');

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Value;
    }
    
    public static RegionIdentifier Parse(int id) => new ((ushort)id);

    public static readonly RegionIdentifier Poland = new(616);
    // ReSharper disable once InconsistentNaming
    public static readonly RegionIdentifier USA = new(840);
    public static readonly RegionIdentifier Missing = new(000);
    public static readonly RegionIdentifier All = new(999);
}

