using PixelPlayInc.Common;

namespace PixelPlayInc_workshop.Common;

public class DateRange : ValueObject<DateRange>
{
    public DateTime From { get; private set; }
    public DateTime To { get; private set; }
    
    [Obsolete("Only for ORM", true)]
    private DateRange(){}

    private DateRange(DateTime from, DateTime to)
    {
        From = from;
        To = to;
    }
    
    public bool IsWithinRange(DateTime date) => date >= From && date <= To;

    public static DateRange Create(DateTime from, DateTime? to)
    {
        var dateTo = to ?? DateTime.MaxValue;
        
        if (from > dateTo)
            throw new ArgumentOutOfRangeException(nameof(from));

        return new DateRange(from, dateTo);
    }
    
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return From;
        yield return To;
    }
}