using PixelPlayInc_workshop.BLL.Dtos;

namespace PixelPlayInc_workshop.BLL;

public interface ILicenceFacade
{
    Task<Guid> CreateLicense(LicenseDto licenseDto, LicensorDto licensorDto);
    Task ActivateLicense(Guid licenseId);
    Task CloseLicense(Guid licenseId);
    Task<LicenseDto> GetLicense(Guid licenseId);
}