
using PixelPlayInc_workshop.License.Application.Port;

namespace PixelPlayInc_workshop.License.Infrastructure.Persistence.Licenses;

public class LicenseRepository: Dictionary<Guid, Domain.License>, ILicenseRepository
{
    public Task Add(Domain.License license, CancellationToken cancellationToken = default)
    {
        Add(license.Id, license);
        
        return Task.CompletedTask;
    }

    public Task Update(Domain.License license, CancellationToken cancellationToken = default)
    {
        this[license.Id] = license;
        
        return Task.CompletedTask;
    }

    public Task<Domain.License?> Get(Guid id, CancellationToken cancellationToken = default)
    {
        var exists = TryGetValue(id, out var license);
        
        return Task.FromResult(exists? license : null);
    }

    // public async Task<LicenseReadModel> GetReadModel(Guid id, ILicensorProvider licensorProvider, CancellationToken cancellationToken = default)
    // {
    //     var exists = TryGetValue(id, out var license);
    //
    //     if (!exists)
    //         throw new Exception("License not found");
    //     
    //     var licensor = await licensorProvider.GetOrCreate(new LicensorIdOrData(license!.LicensorId, null, null), cancellationToken);
    //     
    //     return LicenseReadModel.Map(license, licensor);
    // }
}
