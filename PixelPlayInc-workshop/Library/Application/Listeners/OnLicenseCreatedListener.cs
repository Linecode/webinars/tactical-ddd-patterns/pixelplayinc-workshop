using MediatR;
using PixelPlayInc_workshop.License.Infrastructure.Notifications.IntegrationsEvents;

namespace PixelPlayInc_workshop.Library.Application.Listeners;

public class OnLicenseCreatedListener(ILibraryService service) : INotificationHandler<LicenseCreated>
{
    public Task Handle(LicenseCreated notification, CancellationToken cancellationToken)
    {
        service.Action();

        return Task.CompletedTask;
    }
}