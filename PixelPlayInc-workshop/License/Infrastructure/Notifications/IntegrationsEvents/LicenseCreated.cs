using MediatR;

namespace PixelPlayInc_workshop.License.Infrastructure.Notifications.IntegrationsEvents;

public record LicenseCreated(Guid LicenseId) : INotification;