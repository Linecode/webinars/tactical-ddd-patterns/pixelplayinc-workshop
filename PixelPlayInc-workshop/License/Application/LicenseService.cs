using PixelPlayInc_workshop.License.Application.Command;
using PixelPlayInc_workshop.License.Application.Port;
using PixelPlayInc_workshop.License.Domain.Policies.Activate;
using PixelPlayInc.Common;

namespace PixelPlayInc_workshop.License.Application;

public interface ILicenseService
{
    Task<Guid> Create(Commands.CreateLicenseCommand command);
}

[ApplicationService]
public class LicenseService : ILicenseService
{
    private readonly ILicenseRepository _licenseRepository;
    private readonly ILicensorProvider _licensorProvider;
    private readonly IEventsPublisher _eventsPublisher;

    public LicenseService(
        ILicenseRepository licenseRepository, 
        ILicensorProvider licensorProvider,
        IEventsPublisher eventsPublisher)
    {
        _licenseRepository = licenseRepository;
        _licensorProvider = licensorProvider;
        _eventsPublisher = eventsPublisher;
    }

    public async Task<Guid> Create(Commands.CreateLicenseCommand command)
    {
        var (dateRange, decryptionKey, licensorIdOrData, region) = command;

        var licensor = await _licensorProvider.GetOrCreate(licensorIdOrData);
        var (license, @event) = Domain.License.Create(new Domain.License.Commands.CreateCommand(dateRange, decryptionKey, licensor, region));
        
        await _licenseRepository.Add(license);

        // transaction!!
        
        await _eventsPublisher.Publish(@event);
        
        return license.Id;
    }

    public async Task Activate(Guid licenseId)
    {
        var license = await _licenseRepository.Get(licenseId);
        
        if (license is null)
            throw new Exception("License not found");
        
        var policy = ActivationPolicies.Default;
        
        var command = new Domain.License.Commands.ActivateCommand(DateTime.UtcNow, policy);
        
        var result = license.Activate(command);
    }
}