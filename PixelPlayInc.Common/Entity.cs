namespace PixelPlayInc.Common;

public abstract class Entity<TIdentifier>
{
    public virtual TIdentifier Id { get; protected set; }
    protected virtual object Actual => this;

    public override bool Equals(object obj)
    {
        if (!(obj is Entity<TIdentifier> other))
            return false;

        if (ReferenceEquals(this, other))
            return true;

        if (Id.Equals(default(TIdentifier)) || other.Id.Equals(default(TIdentifier)))
            return false;

        return Id.Equals(other.Id);
    }

    public static bool operator ==(Entity<TIdentifier> a, Entity<TIdentifier> b)
    {
        if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
            return true;

        if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
            return false;

        return a.Equals(b);
    }

    public static bool operator !=(Entity<TIdentifier> a, Entity<TIdentifier> b)
        => !(a == b);

    public override int GetHashCode()
        =>  (Actual.GetType() + Id.ToString()).GetHashCode();
}
