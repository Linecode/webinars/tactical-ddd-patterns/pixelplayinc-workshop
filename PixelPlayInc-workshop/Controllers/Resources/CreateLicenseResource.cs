using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace PixelPlayInc_workshop.Controllers.Resources;

public class CreateLicenseResource
{
    [JsonPropertyName("from")]
    public DateTime StartDate { get; set; }
    
    [JsonPropertyName("to")]
    public DateTime EndDate { get; set; }
    
    public Guid LicensorId { get; set; }
    
    public int Territory { get; set; }
    
    [MinLength(40)]
    [MaxLength(40)]
    public string DecryptionKey { get; set; }
    
    public LicensorResource Licensor { get; set; }
}