using PixelPlayInc_workshop.DAL;

namespace PixelPlayInc_workshop.BLL;

public interface ILibraryService
{
    Task OnLicenseActivated(DAL.License license);
}