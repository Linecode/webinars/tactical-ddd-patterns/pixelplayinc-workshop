using PixelPlayInc_workshop.DAL;

namespace PixelPlayInc_workshop.BLL.Dtos;

public class LicenseDto
{
    public Guid Id { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public Guid LicensorId { get; set; }
    public int Territory { get; set; }
    public string Key { get; set; }
    public LicenseStatus Status { get; set; }
}