namespace PixelPlayInc_workshop.DAL.Repositories;

public interface ILicenseRepository
{
    Task Add(License license, CancellationToken cancellationToken = default);
    Task Update(License license, CancellationToken cancellationToken = default);
    Task<License?> Get(Guid id, CancellationToken cancellationToken = default);
}