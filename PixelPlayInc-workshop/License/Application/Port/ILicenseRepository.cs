namespace PixelPlayInc_workshop.License.Application.Port;

public interface ILicenseRepository
{
    Task Add(Domain.License license, CancellationToken cancellationToken = default);
    
    Task Update(Domain.License license, CancellationToken cancellationToken = default);
    
    Task<Domain.License?> Get(Guid id, CancellationToken cancellationToken = default);

    // Task<LicenseReadModel> GetReadModel(Guid id, ILicensorProvider licensorProvider,
    //     CancellationToken cancellationToken = default);
}
