using PixelPlayInc_workshop.BLL.Dtos;
using PixelPlayInc_workshop.DAL;
using PixelPlayInc_workshop.DAL.Repositories;

namespace PixelPlayInc_workshop.BLL;

public class LicensorService : ILicensorService
{
    private readonly ILicensorRepository _repository;

    public LicensorService(ILicensorRepository repository)
    {
        _repository = repository;
    }

    public async Task<Licensor> GetOrCreate(LicensorDto licensorDto)
    {
        Licensor licensor;

        if (!licensorDto.Id.HasValue)
        {
            licensor = new Licensor
            {
                Id = Guid.NewGuid(),
                Name = licensorDto.Name,
                Address = licensorDto.Address
            };
            await _repository.Add(licensor);
            return licensor;
        }

        if (licensorDto.Id.Value == Guid.Empty)
        {
            licensor = new Licensor
            {
                Id = licensorDto.Id.Value,
                Name = licensorDto.Name,
                Address = licensorDto.Address
            };
            await _repository.Add(licensor);
            return licensor;
        }

        licensor = await _repository.Get(licensorDto.Id.Value);
        return licensor;
    }
}