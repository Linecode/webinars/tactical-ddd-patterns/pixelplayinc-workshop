using PixelPlayInc_workshop.BLL.Dtos;
using PixelPlayInc_workshop.DAL;

namespace PixelPlayInc_workshop.BLL;

public interface ILicensorService
{
    Task<Licensor> GetOrCreate(LicensorDto licensorDto);
}