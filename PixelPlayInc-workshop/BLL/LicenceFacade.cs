using PixelPlayInc_workshop.BLL.Dtos;
using PixelPlayInc_workshop.BLL.Interfaces;

namespace PixelPlayInc_workshop.BLL;

public class LicenceFacade : ILicenceFacade
{
    private readonly ILicenseService _licenseService;
    private readonly ILicensorService _licensorService;
    private readonly ILibraryService _libraryService;
    private readonly ILogger<LicenceFacade> _logger;

    public LicenceFacade(
        ILicenseService licenseService, 
        ILicensorService licensorService, 
        ILibraryService libraryService,
        ILogger<LicenceFacade> logger)
    {
        _licenseService = licenseService;
        _licensorService = licensorService;
        _libraryService = libraryService;
        _logger = logger;
    }

    public async Task<Guid> CreateLicense(LicenseDto licenseDto, LicensorDto licensorDto)
    {
        var licensor = await _licensorService.GetOrCreate(licensorDto);
        
        licenseDto.LicensorId = licensor.Id;

        return await _licenseService.Create(licenseDto);
    }
    
    public async Task ActivateLicense(Guid licenseId)
    {
        var license = await _licenseService.Get(licenseId);
        
        if (license is null)
            throw new Exception("License not found");
        
        await _licenseService.Activate(licenseId);

        await _libraryService.OnLicenseActivated(license);
    }

    public async Task CloseLicense(Guid licenseId)
    {
        var license = await _licenseService.Get(licenseId);
        
        if (license is null)
            throw new Exception("License not found");
        
        await _licenseService.CloseLicense(licenseId);
    }
    
    public async Task<LicenseDto> GetLicense(Guid licenseId)
    {
        var license = await _licenseService.Get(licenseId);

        return new LicenseDto
        {
            Id = license.Id,
            Status = license.Status,
            StartDate = license.StartDate,
            EndDate = license.EndDate,
            Key = license.Key,
            Territory = license.Territory,
            LicensorId = license.LicensorId
        };
    }
}