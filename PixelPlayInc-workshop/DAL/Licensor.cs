namespace PixelPlayInc_workshop.DAL;

public class Licensor : BaseEntity
{
    public string Name { get; set; }
    public string Address { get; set; }
}