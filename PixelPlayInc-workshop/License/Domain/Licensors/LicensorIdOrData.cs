using EnsureThat;

namespace PixelPlayInc_workshop.License.Domain.Licensors;

public record LicensorIdOrData
{
    public Guid? LicensorId { get; }
    public string? Name { get; }
    public string? Address { get; }
    
    public LicensorIdOrData(Guid? licensorId, string? name, string? address)
    {
        if (licensorId is null || licensorId == Guid.Empty)
        {
            Ensure.That(name).IsNotEmptyOrWhiteSpace();
            Ensure.That(address).IsNotEmptyOrWhiteSpace();
        }

        LicensorId = licensorId;
        Name = name;
        Address = address;
    }
}