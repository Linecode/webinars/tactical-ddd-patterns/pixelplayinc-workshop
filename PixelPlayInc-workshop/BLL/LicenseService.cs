using PixelPlayInc_workshop.BLL.Dtos;
using PixelPlayInc_workshop.BLL.Interfaces;
using PixelPlayInc_workshop.DAL;
using PixelPlayInc_workshop.DAL.Repositories;

namespace PixelPlayInc_workshop.BLL;

public class LicenseService : ILicenseService
{
    private readonly ILicenseRepository _licenseRepository;

    public LicenseService(ILicenseRepository licenseRepository)
    {
        _licenseRepository = licenseRepository;
    }
    
    public async Task<Guid> Create(LicenseDto licenseDto)
    {
        var license = new DAL.License
        {
            Id = Guid.NewGuid(),
            Status = LicenseStatus.Provisioned,
            StartDate = licenseDto.StartDate,
            EndDate = licenseDto.EndDate,
            Key = licenseDto.Key,
            Territory = licenseDto.Territory,
            LicensorId = licenseDto.LicensorId
        };

        await _licenseRepository.Add(license);
        
        return license.Id;
    }

    public async Task Activate(Guid licenseId)
    {
        var license = await _licenseRepository.Get(licenseId);
        
        if (license is null)
            throw new Exception("License not found");
        
        if (license.Status != LicenseStatus.Provisioned)
            throw new Exception("License already activated");
        
        if (license.StartDate > DateTime.Now || license.EndDate < DateTime.Now)
            throw new Exception("License not active yet");
        
        license.Status = LicenseStatus.Active;
        
        await _licenseRepository.Update(license);
    }

    public async Task CloseLicense(Guid licenseId)
    {
        var license = await _licenseRepository.Get(licenseId);
        
        if (license is null)
            throw new Exception("License not found");
        
        license.Status = LicenseStatus.Expired;
        
        await _licenseRepository.Update(license);
    }

    public async Task<DAL.License> Get(Guid licenseId)
    {
        return await _licenseRepository.Get(licenseId);
    }

    // public void PrimitiveObssesion(Category category, Section section)
    // {
    //     
    // }
    //
    // public void Main()
    // {
    //     var category = new Category(1);
    //     var section = new Section(2);
    //     
    //     PrimitiveObssesion(section, category);
    // }
}