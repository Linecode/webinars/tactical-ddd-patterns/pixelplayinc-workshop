using MediatR;
using PixelPlayInc_workshop.License.Application.Port;
using PixelPlayInc_workshop.License.Infrastructure.Notifications.IntegrationsEvents;
using PixelPlayInc.Common;

namespace PixelPlayInc_workshop.License.Infrastructure.Notifications;

public class NotificationPublisher(IPublisher publisher) : IEventsPublisher
{
    public async Task Publish<T>(IEnumerable<T> events, CancellationToken ct = default) where T : DomainEvent
    {
        foreach (var @event in events)
        {
            await Publish(@event, ct);
        }
    }

    public async Task Publish<T>(T @event, CancellationToken ct = default) where T : DomainEvent
    {
        var notification = MapFromDomainEvent(@event);
        
        if (notification is not null)
            await publisher.Publish(notification, ct);
    }

    private static INotification? MapFromDomainEvent(DomainEvent domainEvent)
    {
        return domainEvent switch
        {
            Domain.License.Events.LicenseCreated @event => new LicenseCreated(@event.LicenseId),
            _ => null
        };
    }
}