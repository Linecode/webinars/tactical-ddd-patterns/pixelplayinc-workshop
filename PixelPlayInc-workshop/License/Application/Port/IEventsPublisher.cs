using PixelPlayInc.Common;

namespace PixelPlayInc_workshop.License.Application.Port;

public interface IEventsPublisher
{
    Task Publish<T>(IEnumerable<T> events, CancellationToken ct = default)
        where T : DomainEvent;

    Task Publish<T>(T @event, CancellationToken ct = default)
        where T : DomainEvent;
}
